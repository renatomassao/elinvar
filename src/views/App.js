import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';

import { updatePressure, updateTemperature, requestRainfallAmount } from '../redux/modules/dashboard';
import Slider from '../components/Slider';
import ColumnChart from '../components/ColumnChart';
import LineChart from '../components/LineChart';

class App extends Component {
  componentDidMount() {
    this.props.requestRainfallAmount();
  }

  render() {
    const {
      temperature,
      pressure,
      rainfallAmount,
      handlePressure,
      handleTemperature
    } = this.props;
    return (
      <div className="App">
        <div>
          <Slider
            title="Pressure [hPa]"
            min={pressure.min}
            max={pressure.max}
            value={pressure.value}
            handleChange={handlePressure}
          />
          <LineChart
            temperature={temperature.value}
            pressure={pressure.value}
            rainfallAmount={rainfallAmount.days}
          />
        </div>
        <div>
          <Slider
            title="Temperature [°C]"
            min={temperature.min}
            max={temperature.max}
            value={temperature.value}
            handleChange={handleTemperature}
          />
          <ColumnChart rainfallAmount={rainfallAmount} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  temperature: state.temperature,
  pressure: state.pressure,
  rainfallAmount: state.rainfallAmount
});

const mapDispatchToProps = (dispatch) => ({
  handlePressure: (value) => dispatch(updatePressure(value)),
  handleTemperature: (value) => dispatch(updateTemperature(value)),
  requestRainfallAmount: () => dispatch(requestRainfallAmount())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
