import React from 'react';
import { Chart } from 'react-google-charts';
import './Chart.css';

const ColumnChart = ({rainfallAmount}) => {
  const data = [
    ['Days', 'Amount'],
    ...rainfallAmount.days.map(
      x => [`${x.day}`, x.amount]
    )
  ];
  return (
    <div className="Chart">
      <Chart
        chartType="ColumnChart"
        data={data}
        options={{
          title: rainfallAmount.request,
          hAxis: { title: 'Days' },
          vAxis: { title: 'Amount [l/m2]' }
        }}
        width="100%"
      />
    </div>
  );
}

export default ColumnChart;
