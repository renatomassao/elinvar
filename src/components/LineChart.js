import React, { Component } from 'react';
import { Chart } from 'react-google-charts';
import './Chart.css';

class LineChart extends Component {
  chanceOfRain(pressure, temperature, amount) {
    const score = Math.log(amount + 1) * Math.log(pressure - 929) * Math.log(temperature -
    9);
    const mean = Math.min(Math.max(score, 0), 100)
    const upper_bound = Math.min(1.5 * mean, 100);
    const lower_bound = Math.max(0.5 * mean, 0);
    return [lower_bound, mean, upper_bound];
  }

  render() {
    const {
      temperature,
      pressure,
      rainfallAmount
    } = this.props;
    const data = [
      ['Days', 'Lower Bound', 'Mean', 'Upper Bound'],
      ...rainfallAmount.map((day) => {
        return [
          day.day,
          ...this.chanceOfRain(pressure, temperature, day.amount)
        ];
      })
    ]
    return (
      <div className="Chart">
        <Chart
          chartType="LineChart"
          data={data}
          options={{
            title: 'Chance of Rain'
          }}
          width="100%"
        />
      </div>
    );
  }
}

export default LineChart;
