import React, { Component } from 'react';
import './Slider.css';

class Slider extends Component {
  handleStart = event => {
    document.addEventListener('mousemove', this.handleDrag);
    document.addEventListener('mouseup', this.handleEnd);
  };

  handlePosition = clientX => {
    const width = this.track.offsetWidth;
    const posX = clientX - this.track.offsetLeft;
    const percentage = Math.round( (posX * 100) / width );
    return percentage < 0 ? 0
            : percentage > 100 ? 100
            : percentage;
  }

  handleDrag = event => {
    event.stopPropagation();
    const percentage = this.handlePosition(event.clientX);
    const value = this.getValue(percentage);
    this.handle.style = `left: ${percentage}%`;
    this.label.style = `left: ${percentage}%`;
    this.label.innerHTML = value;
  };

  handleEnd = event => {
    document.removeEventListener('mousemove', this.handleDrag);
    document.removeEventListener('mouseup', this.handleEnd);
    const percentage = this.handlePosition(event.clientX);
    const value = this.getValue(percentage);
    this.props.handleChange(value);
  };

  getValue = position => {
    const { min, max } = this.props;
    const range = max - min;
    let value = Math.round((range * position) / 100);
    return value + parseInt(min, 10);
  }

  getPosition = value => {
    const { min, max } = this.props;
    const range = max - min;
    const diffValMin = value - min;
    const percentage = diffValMin / range;
    return Math.round(percentage * 100);
  };

  render() {
    const {
      title,
      value
    } = this.props;
    const position = this.getPosition(value);
    const handleStyle = { left: `${position}%` }
    return (
      <div className="Slider">
        <h2 className="Title">{title}</h2>
        <div className="Track"
          ref={track => this.track = track}
          onMouseDown={this.handleDrag}
          onMouseUp={this.handleEnd}
        >
          <div className="Handle"
            onMouseDown={this.handleStart}
            onTouchMove={this.handleDrag}
            onTouchEnd={this.handleEnd}
            style={handleStyle}
            ref={handle => this.handle = handle}
          />
          <span className="Label" ref={label => this.label = label}>
            {value}
          </span>
        </div>
      </div>
    );
  };
}

export default Slider;
