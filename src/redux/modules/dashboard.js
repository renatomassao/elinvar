export const UPDATE_PRESSURE = 'dashboard/UPDATE_PRESSURE';
export const UPDATE_TEMPERATURE = 'dashboard/UPDATE_TEMPERATURE';
export const SET_RAINFALL_AMOUNT = 'dashvoard/SET_RAINFALL_AMOUNT';

export const updatePressure = (value) => ({
  type: UPDATE_PRESSURE,
  payload: value
});

export const updateTemperature = (value) => ({
  type: UPDATE_TEMPERATURE,
  payload: value
});

export const setRainfallAmount = (value) => ({
  type: SET_RAINFALL_AMOUNT,
  payload: value
});

export const requestRainfallAmount = () =>
  (dispatch) =>
    fetch('http://private-4945e-weather34.apiary-proxy.com/weather34/rain')
    .then(res => res.json())
    .then(
      rainfallAmount => {
        dispatch(
          setRainfallAmount(rainfallAmount[0])
        )
      }
    );

const initialState = {
  pressure: {
    value: 970,
    max: 1030,
    min: 970
  },
  temperature: {
    value: 10,
    max: 35,
    min: 10
  },
  rainfallAmount: {
    request: '',
    days: []
  }
}

export default (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_PRESSURE:
      return {
        ...state,
        pressure: {
          ...state.pressure,
          value: action.payload
        }
      };
    case UPDATE_TEMPERATURE:
      return {
        ...state,
        temperature: {
          ...state.temperature,
          value: action.payload
        }
      };
    case SET_RAINFALL_AMOUNT:
      return {
        ...state,
        rainfallAmount: action.payload
      };
    default:
      return state;
  }
}


